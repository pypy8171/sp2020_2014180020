#version 450

//layout(location = 0)in vec3 a_Position;
//in vec4 a_Color; // vs input 선언, float 4개

in vec3 a_Position;
in vec3 a_Dir;

//out vec4 v_Color;
uniform float u_Scale;
//uniform float u_Timer;
uniform float u_Speed;

////
in vec3 a_Vel;
uniform float u_Time;
vec3 c_Gravity = vec3(0, -0.25,0);
in float a_StartTime;
in float a_LifeTime;
in float a_Period;
in float a_Amp;
in float a_Value;

bool c_bLoop= true;
////

float c_PI = 3.141592;

in vec4 a_Color; // vs input 선언, flaot 4개, in->vs input->attrib
out vec4 v_Color;

void main()
{
	float newTime = u_Time - a_StartTime;
	vec2 initParametricPos = vec2(0.7f * sin(a_Value*2.0*c_PI),0.7f *cos( a_Value*2.0*c_PI));
	vec4 newPos = vec4(a_Position.xy + initParametricPos, 0,1);

	float period = a_Period;
	float amp = a_Amp;
	float alpha = 0.0;

	if(newTime >0)
	{
		float tempTime = newTime;
		if(c_bLoop)
		{
			alpha = fract(tempTime/a_LifeTime);
			tempTime = alpha*a_LifeTime; // 이 의미 , 죽지직전 1, 시작 0 -> a값으로 이용
		}

		newPos.xyz = newPos.xyz + a_Vel.xyz *tempTime + 0.5*c_Gravity*tempTime*tempTime;

		vec2 rotVel = vec2(-(a_Vel.y + c_Gravity.y*tempTime), (a_Vel.x + c_Gravity.x*tempTime));
		rotVel = normalize(rotVel);
		newPos.xy = newPos.xy + tempTime* rotVel*amp*sin(2.0*c_PI*a_Period);
		
	
	}
	else
	{
		newPos = vec4(-100000,100000,0,1);
	}

	gl_Position = newPos;
	v_Color = vec4(a_Color.rgb,(1.0  - alpha)); // 랜덤 컬러
}

	/*float newTime = u_Time - a_StartTime;
	vec4 newPos = vec4(a_Position,1);

	float period = a_Period;
	float amp = a_Amp;

	if( newTime > 0 )
	{
		float tempTime = newTime;
		if(c_bLoop)
			tempTime = fract(tempTime/a_LifeTime)*a_LifeTime; //fract 소수점 땜 // hw 가속 가능성 커짐 gpu // floor 내림 , round 반올림, ceil 올림

		
		newPos.x = a_Position.x + tempTime; // 0~증가
		newPos.y = a_Position.y +  tempTime * amp * sin( 2.0 * c_PI * period / tempTime); // 주기를 줄일라면 *2 이런거 해주면 될듯 // 
		//newpos.y = a_position.y + tempTime * amp * sin(tempTime*2*c_pi*period); // 에너지파느낌 
		newPos.z = 0.0;
		newPos.w = 1.0;
	}
	else
	{
	}

	gl_Position = newPos;
	v_Color = vec4(1,0,0,1);*/



	//newPos.x = a_Position.x + tempTime;
		//newPos.y = a_Position.y + tempTime *amp * sin(tempTime * 2.0 * c_PI * period); // 진동 주기를 점점더 길게 -> 시험
		//newPos.z = 0.0;
		//newPos.w = 1.0;

		// clamp -> intrinsic function -> reference card download, pdf