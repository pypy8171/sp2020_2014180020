#version 450

layout(location=0) out vec4 FragColor;

in vec4 v_Color;

in vec2 v_TexCoord;

void main()
{
	FragColor = vec4(v_TexCoord,0.f,1.f);
}
