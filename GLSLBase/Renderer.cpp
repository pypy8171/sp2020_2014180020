#include "stdafx.h"
#include "Renderer.h"
#include "LoadPng.h"
#include <Windows.h>
#include <cstdlib>
#include <cassert>



Renderer::Renderer(int windowSizeX, int windowSizeY)
{
	Initialize(windowSizeX, windowSizeY);
}


Renderer::~Renderer()
{
}

void Renderer::Initialize(int windowSizeX, int windowSizeY)
{
	//Set window size
	m_WindowSizeX = windowSizeX;
	m_WindowSizeY = windowSizeY;

	//Load shaders
	m_SolidRectShader = CompileShaders("./Shaders/SolidRect.vs", "./Shaders/SolidRect.fs");
	m_FSSandboxShader = CompileShaders("./Shaders/FSSandbox.vs", "./Shaders/FSSandbox.fs");
	//Create VBOs
	CreateVertexBufferObjects();
}
float g_fSin = 0.f;
void Renderer::CreateVertexBufferObjects()
{
	float rect[]
		=
	{
		-0.5, -0.5, 0.f, -0.5, 0.5, 0.f, 0.5, 0.5, 0.f, //Triangle1
		-0.5, -0.5, 0.f,  0.5, 0.5, 0.f, 0.5, -0.5, 0.f, //Triangle2
	};

	glGenBuffers(1, &m_VBORect);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBORect);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_STATIC_DRAW);

	float rectFSSandbox[]
		=
	{
		-0.5, -0.5, 0.f, -0.5, 0.5, 0.f, 0.5, 0.5, 0.f, //Triangle1
		-0.5, -0.5, 0.f,  0.5, 0.5, 0.f, 0.5, -0.5, 0.f, //Triangle2
	};

	glGenBuffers(1, &m_VBOFSSandbox);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOFSSandbox);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectFSSandbox), rectFSSandbox, GL_STATIC_DRAW);

	//float vertices[]
	//	=
	//{
	//	-0.7f,-0.7f,0.f,-0.7f,0.7f,0.f, // 0,1
	//	-0.3f,0.7f,0.f,-0.3f,0.f,0.f, -0.7f, 0.f,0.f, // 2,3,4 P 0~4
	//	-0.2f,0.7f,0.f,0.f,0.f,0.f,0.2f,0.7f,0.f, // 5,6,7
	//	0.f,0.f,0.f,0.f,-0.7f,0.f, // 8,9 // Y 5~9
	//	0.3f,0.7f,0.f,0.3f,-0.7f,0.f, // 10, 11 
	//	0.7f,0.7f,0.f,0.7f,-0.7f,0.f, // 12 13
	//	0.3f,0.f,0.f,0.7f,0.f,0.f // 14 15 // H 10~15

	//};

	//glGenBuffers(1, &m_VBOTest);
	//glBindBuffer(GL_ARRAY_BUFFER, m_VBOTest);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	//g_fSin += 0.1f;

	float vertices2[]
		=
	{
		0.f,0.f,0.f,
		1.f,0.f,0.f,
		1.f,1.f,0.f,
	};

	glGenBuffers(1, &m_VBOTest);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTest);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices2), vertices2, GL_STATIC_DRAW);

	float color[]
		=
	{
		1,0,0,1,
		0,1,0,1,
		0,0,1,1,
	};

	glGenBuffers(1, &m_VBOTestColor);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTestColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);

	float particlesize = 0.01f;

	float singleParticleVertices[] // 삼각형 두개짜리 사각형
		=
	{
		-1.f * particlesize,-1.f * particlesize,0.f,
		1.f * particlesize,1.f * particlesize,0.f,
		-1.f * particlesize,1.f * particlesize,0.f,
		-1.f * particlesize,-1.f * particlesize,0.f,
		1.f * particlesize, -1.f * particlesize,0.f,
		1.f * particlesize,1.f * particlesize,0.f,
	}; // cpu memory 에 있음 -> gpu memory로 옮겨야함

	glGenBuffers(1, &m_VBOSingleParticle); // just generate id
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOSingleParticle); // 용도 부여
	glBufferData(GL_ARRAY_BUFFER, sizeof(singleParticleVertices), singleParticleVertices, GL_STATIC_DRAW);


	//createparticle
	CreateParticle(10000);
}

void Renderer::AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType)
{
	//쉐이더 오브젝트 생성
	GLuint ShaderObj = glCreateShader(ShaderType);

	if (ShaderObj == 0) {
		fprintf(stderr, "Error creating shader type %d\n", ShaderType);
	}

	const GLchar* p[1];
	p[0] = pShaderText;
	GLint Lengths[1];
	Lengths[0] = (GLint)strlen(pShaderText);
	//쉐이더 코드를 쉐이더 오브젝트에 할당
	glShaderSource(ShaderObj, 1, p, Lengths);

	//할당된 쉐이더 코드를 컴파일
	glCompileShader(ShaderObj);

	GLint success;
	// ShaderObj 가 성공적으로 컴파일 되었는지 확인
	glGetShaderiv(ShaderObj, GL_COMPILE_STATUS, &success);
	if (!success) {
		GLchar InfoLog[1024];

		//OpenGL 의 shader log 데이터를 가져옴
		glGetShaderInfoLog(ShaderObj, 1024, NULL, InfoLog);
		fprintf(stderr, "Error compiling shader type %d: '%s'\n", ShaderType, InfoLog);
		printf("%s \n", pShaderText);
	}

	// ShaderProgram 에 attach!!
	glAttachShader(ShaderProgram, ShaderObj);
}

bool Renderer::ReadFile(char* filename, std::string *target)
{
	std::ifstream file(filename);
	if (file.fail())
	{
		std::cout << filename << " file loading failed.. \n";
		file.close();
		return false;
	}
	std::string line;
	while (getline(file, line)) {
		target->append(line.c_str());
		target->append("\n");
	}
	return true;
}

GLuint Renderer::CompileShaders(char* filenameVS, char* filenameFS)
{
	GLuint ShaderProgram = glCreateProgram(); //빈 쉐이더 프로그램 생성

	if (ShaderProgram == 0) { //쉐이더 프로그램이 만들어졌는지 확인
		fprintf(stderr, "Error creating shader program\n");
	}

	std::string vs, fs;

	//shader.vs 가 vs 안으로 로딩됨
	if (!ReadFile(filenameVS, &vs)) {
		printf("Error compiling vertex shader\n");
		return -1;
	};

	//shader.fs 가 fs 안으로 로딩됨
	if (!ReadFile(filenameFS, &fs)) {
		printf("Error compiling fragment shader\n");
		return -1;
	};

	// ShaderProgram 에 vs.c_str() 버텍스 쉐이더를 컴파일한 결과를 attach함
	AddShader(ShaderProgram, vs.c_str(), GL_VERTEX_SHADER);

	// ShaderProgram 에 fs.c_str() 프레그먼트 쉐이더를 컴파일한 결과를 attach함
	AddShader(ShaderProgram, fs.c_str(), GL_FRAGMENT_SHADER);

	GLint Success = 0;
	GLchar ErrorLog[1024] = { 0 };

	//Attach 완료된 shaderProgram 을 링킹함
	glLinkProgram(ShaderProgram);

	//링크가 성공했는지 확인
	glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &Success);

	if (Success == 0) {
		// shader program 로그를 받아옴
		glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << filenameVS << ", " << filenameFS << " Error linking shader program\n" << ErrorLog;
		return -1;
	}

	glValidateProgram(ShaderProgram);
	glGetProgramiv(ShaderProgram, GL_VALIDATE_STATUS, &Success);
	if (!Success) {
		glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << filenameVS << ", " << filenameFS << " Error validating shader program\n" << ErrorLog;
		return -1;
	}

	glUseProgram(ShaderProgram);
	std::cout << filenameVS << ", " << filenameFS << " Shader compiling is done.\n";

	return ShaderProgram;
}
unsigned char * Renderer::loadBMPRaw(const char * imagepath, unsigned int& outWidth, unsigned int& outHeight)
{
	std::cout << "Loading bmp file " << imagepath << " ... " << std::endl;
	outWidth = -1;
	outHeight = -1;
	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file = NULL;
	fopen_s(&file, imagepath, "rb");
	if (!file)
	{
		std::cout << "Image could not be opened, " << imagepath << " is missing. " << std::endl;
		return NULL;
	}

	if (fread(header, 1, 54, file) != 54)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (header[0] != 'B' || header[1] != 'M')
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (*(int*)&(header[0x1E]) != 0)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (*(int*)&(header[0x1C]) != 24)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	outWidth = *(int*)&(header[0x12]);
	outHeight = *(int*)&(header[0x16]);

	if (imageSize == 0)
		imageSize = outWidth * outHeight * 3;

	if (dataPos == 0)
		dataPos = 54;

	data = new unsigned char[imageSize];

	fread(data, 1, imageSize, file);

	fclose(file);

	std::cout << imagepath << " is succesfully loaded. " << std::endl;

	return data;
}

GLuint Renderer::CreatePngTexture(char * filePath)
{
	//Load Pngs: Load file and decode image.
	std::vector<unsigned char> image;
	unsigned width, height;
	unsigned error = lodepng::decode(image, width, height, filePath);
	if (error != 0)
	{
		lodepng_error_text(error);
		assert(error == 0);
		return -1;
	}

	GLuint temp;
	glGenTextures(1, &temp);

	glBindTexture(GL_TEXTURE_2D, temp);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);

	return temp;
}

GLuint Renderer::CreateBmpTexture(char * filePath)
{
	//Load Bmp: Load file and decode image.
	unsigned int width, height;
	unsigned char * bmp
		= loadBMPRaw(filePath, width, height);

	if (bmp == NULL)
	{
		std::cout << "Error while loading bmp file : " << filePath << std::endl;
		assert(bmp != NULL);
		return -1;
	}

	GLuint temp;
	glGenTextures(1, &temp);

	glBindTexture(GL_TEXTURE_2D, temp);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bmp);

	return temp;
}


void Renderer::CreateParticle(int count)
{
	int floatCount = count * 3 * 15 * 2; // 3 -> 6
	int vertexCount = count * 3 * 2;
	float* particleVertices = new float[floatCount];

	int index = 0;
	float particlesize = 0.02f;

	for (int i = 0; i < count; ++i)
	{
		// quad

		float randomvaluex = 0.f;
		float randomvaluey = 0.f;
		float randomvaluez = 0.f;

		float randomvelocityx = 0.f;
		float randomvelocityy = 0.f;
		float randomvelocityz = 0.f;

		float randomstarttime = 0.f;
		float randomlifetime = 0.f;
		float randomperiod = 0.f;
		float randomamp = 0.f;

		float randomvalue = 0.f;
		float randomR, randomG, randomB, randomA = 0.f;

		randomvaluex = 0.f; // (rand() / (float)RAND_MAX - 0.5) * 2.f;
		randomvaluey = 0.f; //  (rand() / (float)RAND_MAX - 0.5) * 2.f;
		randomvaluez = 0.f;

		randomvelocityx =  (rand() / (float)RAND_MAX - 0.5f) * 0.5f;
		randomvelocityy =  (rand() / (float)RAND_MAX - 0.5f) * 0.5f;
		randomvelocityz = 0.f;

		randomstarttime = (rand() / (float)RAND_MAX) * 6.f;
		randomlifetime = (rand() / (float)RAND_MAX) * 3.f;

		randomperiod = (rand() / (float)RAND_MAX) * 0.5f;
		randomamp = (rand() / (float)RAND_MAX) * 0.f;

		randomvalue = (rand() / (float)RAND_MAX) * 1.f;

		randomR = (rand() / (float)RAND_MAX);
		randomG = (rand() / (float)RAND_MAX);
		randomB = (rand() / (float)RAND_MAX);
		randomA = (rand() / (float)RAND_MAX);
		// v0
		particleVertices[index] = -particlesize / 2.f + randomvaluex;;
		++index;
		particleVertices[index] = -particlesize / 2.f + randomvaluey;;
		++index;
		particleVertices[index] = 0.f; // z값에 랜덤값 넣고 속도 써도 되나
		++index;
		particleVertices[index] = randomvelocityx;
		++index;
		particleVertices[index] = randomvelocityy;
		++index;
		particleVertices[index] = randomvelocityz;;
		++index;
		particleVertices[index] = randomstarttime;;
		++index;
		particleVertices[index] = randomlifetime;;
		++index;
		particleVertices[index] = randomperiod;;
		++index;
		particleVertices[index] = randomamp;;
		++index;
		particleVertices[index] = randomvalue;;
		++index;
		particleVertices[index] = randomR;;
		++index;
		particleVertices[index] = randomG;;
		++index;
		particleVertices[index] = randomB;;
		++index;
		particleVertices[index] = randomA;;
		++index;

		// v1
		particleVertices[index] = particlesize / 2.f + randomvaluex;;
		++index;
		particleVertices[index] = -particlesize / 2.f + randomvaluey;;
		++index;
		particleVertices[index] = 0.f; // z값에 랜덤값 넣고 속도 써도 되나
		++index;
		particleVertices[index] = randomvelocityx;
		++index;
		particleVertices[index] = randomvelocityy;
		++index;
		particleVertices[index] = randomvelocityz;;
		++index;
		particleVertices[index] = randomstarttime;;
		++index;
		particleVertices[index] = randomlifetime;;
		++index;
		particleVertices[index] = randomperiod;;
		++index;
		particleVertices[index] = randomamp;;
		++index;
		particleVertices[index] = randomvalue;;
		++index;
		particleVertices[index] = randomR;;
		++index;
		particleVertices[index] = randomG;;
		++index;
		particleVertices[index] = randomB;;
		++index;
		particleVertices[index] = randomA;;
		++index;

		// v2
		particleVertices[index] = particlesize / 2.f + randomvaluex;;
		++index;
		particleVertices[index] = particlesize / 2.f + randomvaluey;;
		++index;
		particleVertices[index] = 0.f; // z값에 랜덤값 넣고 속도 써도 되나
		++index;
		particleVertices[index] = randomvelocityx;
		++index;
		particleVertices[index] = randomvelocityy;
		++index;
		particleVertices[index] = randomvelocityz;;
		++index;
		particleVertices[index] = randomstarttime;;
		++index;
		particleVertices[index] = randomlifetime;;
		++index;
		particleVertices[index] = randomperiod;;
		++index;
		particleVertices[index] = randomamp;;
		++index;
		particleVertices[index] = randomvalue;;
		++index;
		particleVertices[index] = randomR;;
		++index;
		particleVertices[index] = randomG;;
		++index;
		particleVertices[index] = randomB;;
		++index;
		particleVertices[index] = randomA;;
		++index;

		// v3
		particleVertices[index] = -particlesize / 2.f + randomvaluex;;
		++index;
		particleVertices[index] = -particlesize / 2.f + randomvaluey;;
		++index;
		particleVertices[index] = 0.f; // z값에 랜덤값 넣고 속도 써도 되나
		++index;
		particleVertices[index] = randomvelocityx;
		++index;
		particleVertices[index] = randomvelocityy;
		++index;
		particleVertices[index] = randomvelocityz;;
		++index;
		particleVertices[index] = randomstarttime;;
		++index;
		particleVertices[index] = randomlifetime;;
		++index;
		particleVertices[index] = randomperiod;;
		++index;
		particleVertices[index] = randomamp;;
		++index;
		particleVertices[index] = randomvalue;;
		++index;
		particleVertices[index] = randomR;;
		++index;
		particleVertices[index] = randomG;;
		++index;
		particleVertices[index] = randomB;;
		++index;
		particleVertices[index] = randomA;;
		++index;

		// v4
		particleVertices[index] = particlesize / 2.f + randomvaluex;;
		++index;
		particleVertices[index] = particlesize / 2.f + randomvaluey;;
		++index;
		particleVertices[index] = 0.f; // z값에 랜덤값 넣고 속도 써도 되나
		++index;
		particleVertices[index] = randomvelocityx;
		++index;
		particleVertices[index] = randomvelocityy;
		++index;
		particleVertices[index] = randomvelocityz;;
		++index;
		particleVertices[index] = randomstarttime;;
		++index;
		particleVertices[index] = randomlifetime;;
		++index;
		particleVertices[index] = randomperiod;;
		++index;
		particleVertices[index] = randomamp;;
		++index;
		particleVertices[index] = randomvalue;;
		++index;
		particleVertices[index] = randomR;;
		++index;
		particleVertices[index] = randomG;;
		++index;
		particleVertices[index] = randomB;;
		++index;
		particleVertices[index] = randomA;;
		++index;

		// v5
		particleVertices[index] = -particlesize / 2.f + randomvaluex;;
		++index;
		particleVertices[index] = particlesize / 2.f + randomvaluey;;
		++index;
		particleVertices[index] = 0.f; // z값에 랜덤값 넣고 속도 써도 되나
		++index;
		particleVertices[index] = randomvelocityx;
		++index;
		particleVertices[index] = randomvelocityy;
		++index;
		particleVertices[index] = randomvelocityz;;
		++index;
		particleVertices[index] = randomstarttime;;
		++index;
		particleVertices[index] = randomlifetime;;
		++index;
		particleVertices[index] = randomperiod;;
		++index;
		particleVertices[index] = randomamp;;
		++index;
		particleVertices[index] = randomvalue;;
		++index;
		particleVertices[index] = randomR;;
		++index;
		particleVertices[index] = randomG;;
		++index;
		particleVertices[index] = randomB;;
		++index;
		particleVertices[index] = randomA;;
		++index;
	}
	glGenBuffers(1, &m_VBOManyParticle);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOManyParticle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * floatCount, particleVertices, GL_STATIC_DRAW);
	m_VBOManyParticleCount = vertexCount;

}

void Renderer::LectureTest(float felapsedTime)
{
	GLuint shader = m_SolidRectShader;

	// prepare step
	glUseProgram(shader);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_BLEND);

	GLuint posId = glGetAttribLocation(shader, "a_Position"); // Vec3 pos
	glEnableVertexAttribArray(posId); // layout
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOManyParticle); // 18개 float 
	glVertexAttribPointer(posId, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 15, 0);

	GLuint velId = glGetAttribLocation(shader, "a_Vel");
	glEnableVertexAttribArray(velId);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOManyParticle);
	glVertexAttribPointer(velId, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float)*3));

	GLuint startTimeId = glGetAttribLocation(shader, "a_StartTime");
	glEnableVertexAttribArray(startTimeId);
	glVertexAttribPointer(startTimeId, 1, GL_FLOAT, GL_FALSE,sizeof(float)*15, (GLvoid*)(sizeof(float)*6));

	GLuint lifeTimeId = glGetAttribLocation(shader, "a_LifeTime");
	glEnableVertexAttribArray(lifeTimeId);
	glVertexAttribPointer(lifeTimeId, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 7));

	GLuint periodId = glGetAttribLocation(shader, "a_Period");
	glEnableVertexAttribArray(periodId);
	glVertexAttribPointer(periodId, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 8));

	GLuint ampId = glGetAttribLocation(shader, "a_Amp");
	glEnableVertexAttribArray(ampId);
	glVertexAttribPointer(ampId, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 9));

	GLuint valueId = glGetAttribLocation(shader, "a_Value");
	glEnableVertexAttribArray(valueId);
	glVertexAttribPointer(valueId, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 10));

	GLuint colorId = glGetAttribLocation(shader, "a_Color");
	glEnableVertexAttribArray(colorId);
	glVertexAttribPointer(colorId, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 11));


	GLuint timerid = glGetUniformLocation(shader, "u_Time");
	glUniform1f(timerid, m_fTotalTimeLecture3);

	//GLuint speedid = glGetUniformLocation(shader, "u_Speed");
	//glUniform1f(speedid, (rand() / (float)RAND_MAX - 0.5));

	glDrawArrays(GL_TRIANGLES, 0, m_VBOManyParticleCount);
	
	m_fTotalTimeLecture3 += felapsedTime;

	glDisable(GL_BLEND);

}
void Renderer::FSSandbox(float elapsedtimeinsec)
{
	GLuint shader = m_FSSandboxShader;
	glUseProgram(shader);

	// prepare step
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOFSSandbox); // 18개 float 
	GLuint posId = glGetAttribLocation(shader, "a_Position"); // Vec3 pos
	glEnableVertexAttribArray(posId); // layout
	glVertexAttribPointer(posId, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glDisable(GL_BLEND);

}
// 파티클 풀이 있어야 할듯
GLuint Renderer::CreateParticle(PARTICLE_TYPE ParticleType, int ParticleNum, GLuint Shader, const char* name, GLuint VBOInfo, int Size)
{
	GLuint id;

	switch (ParticleType)
	{
	case PARTICLE_TYPE::VERTEX:
	{
		id = glGetAttribLocation(Shader, name);
		glEnableVertexAttribArray(id); // layout
		glBindBuffer(GL_ARRAY_BUFFER, VBOInfo); // 18개 float 

		return id; // 파티클 풀에 넣어서 배열을 넘겨준다?
	}
	case PARTICLE_TYPE::UNIFORM:
	{
		id = glGetUniformLocation(Shader, name);
		return id;
	}
	break;
	default:
		return false;
	}

}


static double fDT = 0.f;

void Renderer::DrawSign()
{
	GLuint shader = m_SolidRectShader;

	// prepare step
	glUseProgram(shader);

	GLuint posId = glGetAttribLocation(shader, "a_Position"); // Vec3 pos
	glEnableVertexAttribArray(posId); // layout
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOSingleParticle); // 18개 float 
	glVertexAttribPointer(posId, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

	fDT += 0.000005f;//duration_cast<milliseconds>(st_t - end_t).count();

	GLuint timerId = glGetUniformLocation(shader, "u_Timer");
	glUniform1f(timerId, fDT);

	//GLuint colId = glGetAttribLocation(shader, "a_Color");
	//glEnableVertexAttribArray(colId); // layout
	//glBindBuffer(GL_ARRAY_BUFFER, m_VBOTestColor);
	//glVertexAttribPointer(colId, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, 0);

	//GLuint scaleID = glGetUniformLocation(shader, "u_Scale");
	//glUniform1f(scaleID, 1.f);

	glDrawArrays(GL_TRIANGLES, 0, 6);


	//int attribPosition = glGetAttribLocation(m_SolidRectShader, "a_Position");
	//glEnableVertexAttribArray(attribPosition); // glEnableVertexAttribArray(0);
	//glBindBuffer(GL_ARRAY_BUFFER, m_VBOTest);
	//glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);
	////glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

	//glDrawArrays(GL_LINES, 0, 2);
	//glDrawArrays(GL_LINES, 1, 2);
	//glDrawArrays(GL_LINES, 2, 2);
	//glDrawArrays(GL_LINES, 3, 2);

	//glDrawArrays(GL_LINES, 5, 2);
	//glDrawArrays(GL_LINES, 6, 2);
	//glDrawArrays(GL_LINES, 8, 2);

	//glDrawArrays(GL_LINES, 10, 2);
	//glDrawArrays(GL_LINES, 12, 2);
	//glDrawArrays(GL_LINES, 14, 2);

	//glDisableVertexAttribArray(attribPosition);
	////glDisableVertexAttribArray(0);
}