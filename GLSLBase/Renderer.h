#pragma once

#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include "Dependencies\glew.h"
#include "Dependencies\wglew.h"
#include "Dependencies\glm/glm.hpp"
#include "Dependencies\glm/gtc/matrix_transform.hpp"
#include "Dependencies\glm/gtx/euler_angles.hpp"

enum class PARTICLE_TYPE{VERTEX,UNIFORM};

class Renderer
{
public:
	Renderer(int windowSizeX, int windowSizeY);
	~Renderer();

	GLuint CreatePngTexture(char * filePath);
	GLuint CreateBmpTexture(char * filePath);

	void DrawSign();
	void LectureTest(float felapsedTime);
	void FSSandbox(float elapsedtimeinsec);

private:
	GLuint CreateParticle(PARTICLE_TYPE ParticleType, int ParticleNum, GLuint Shader, const char* name, GLuint VBOInfo, int Size);

	void CreateParticle(int count);

private:
	void Initialize(int windowSizeX, int windowSizeY);
	bool ReadFile(char* filename, std::string *target);
	void AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType);
	GLuint CompileShaders(char* filenameVS, char* filenameFS);
	void CreateVertexBufferObjects(); 
	unsigned char * Renderer::loadBMPRaw(const char * imagepath, unsigned int& outWidth, unsigned int& outHeight);

	bool m_Initialized = false;
	
	unsigned int m_WindowSizeX = 0;
	unsigned int m_WindowSizeY = 0;

	GLuint m_FSSandboxShader = 0;
	GLuint m_VBOFSSandbox = 0;

	GLuint m_VBORect = 0;
	GLuint m_SolidRectShader = 0;

	GLuint m_VBOTest = 0;
	GLuint m_VBOTestColor = 0;

	GLuint m_VBOSingleParticle = 0;

	float m_fTotalTimeLecture3 = 0;
	//
	GLuint	m_VBOManyParticle = 0;
	GLuint	m_VBOManyParticleCount = 0;
};

